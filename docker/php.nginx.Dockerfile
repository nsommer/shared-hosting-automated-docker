ARG ALPINE_VERSION="latest"

FROM alpine:${ALPINE_VERSION}

# Run as root
USER root

# Install base packages
RUN apk add --no-cache \
    tini \
    fuse \
    wget \
    mysql-client \
    nodejs \
    npm \
    patch \
    gettext \
    git

# Install PHP
ARG PHP_VERSION="81"
RUN apk add --no-cache --update \
    php${PHP_VERSION} \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-pecl-apcu \
    php${PHP_VERSION}-bcmath \
    php${PHP_VERSION}-bz2 \
    php${PHP_VERSION}-exif \
    php${PHP_VERSION}-soap \
    php${PHP_VERSION}-gettext \
    php${PHP_VERSION}-xsl \
    php${PHP_VERSION}-cgi \
    php${PHP_VERSION}-ctype \
    php${PHP_VERSION}-curl \
    php${PHP_VERSION}-dom \
    php${PHP_VERSION}-fpm \
    php${PHP_VERSION}-ftp \
    php${PHP_VERSION}-gd \
    php${PHP_VERSION}-iconv \
    php${PHP_VERSION}-json \
    php${PHP_VERSION}-mbstring \
    php${PHP_VERSION}-opcache \
    php${PHP_VERSION}-openssl \
    php${PHP_VERSION}-pcntl \
    php${PHP_VERSION}-pecl-msgpack \
    php${PHP_VERSION}-pecl-imagick \
    php${PHP_VERSION}-pecl-igbinary \
    php${PHP_VERSION}-pdo \
    php${PHP_VERSION}-pdo_mysql \
    php${PHP_VERSION}-phar \
    php${PHP_VERSION}-redis \
    php${PHP_VERSION}-session \
    php${PHP_VERSION}-simplexml \
    php${PHP_VERSION}-tokenizer \
    php${PHP_VERSION}-xml \
    php${PHP_VERSION}-xmlwriter \
    php${PHP_VERSION}-xmlreader \
    php${PHP_VERSION}-zip \
    php${PHP_VERSION}-fileinfo \
    php${PHP_VERSION}-intl \
    php${PHP_VERSION}-sodium \
    php${PHP_VERSION}-zlib; \
    if [ "${PHP_VERSION}" = "81" ]; then \
        ln -s /usr/bin/php${PHP_VERSION} /usr/bin/php; \
        ln -s /usr/sbin/php-fpm${PHP_VERSION} /usr/bin/php-fpm; \
    else \
        ln -s /usr/sbin/php-fpm${PHP_VERSION} /usr/bin/php-fpm; \
    fi; \
    # Fix php.ini settings for enabled extensions
    chmod +x "$(php -r 'echo ini_get("extension_dir");')"/* && \
    # Install composer
    wget https://getcomposer.org/download/latest-2.x/composer.phar && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +x /usr/local/bin/composer && \
    # Shrink binaries
    (find /usr/local/bin -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    (find /usr/local/lib -type f -print0 | xargs -n1 -0 strip --strip-all -p 2>/dev/null || true) && \
    # Cleanup
    rm -rf /tmp/* /src

# Install nginx
RUN apk add --no-cache --update \
    nginx \
    && \
    mkdir -p /var/cache/nginx

# Set workdir
WORKDIR /var/www

# Add golang version of supervisor
COPY --from=docker.io/ochinchina/supervisord:latest /usr/local/bin/supervisord /usr/local/bin/supervisord
COPY docker/config/supervisor/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Copy in production configs
ARG PHP_VERSION="81"
COPY docker/config/fpm/www.conf /etc/php${PHP_VERSION}/php-fpm.d/www.conf
COPY docker/config/fpm/php-fpm.conf /etc/php${PHP_VERSION}/php-fpm.conf
COPY docker/config/fpm/php.ini /etc/php${PHP_VERSION}/php.ini
COPY docker/config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY docker/config/nginx/nginx.www.conf.tmpl /etc/nginx/conf.d/www.conf.tmpl

# Copy in entrypoint
COPY docker/config/fpm/php-fpm.sh /entrypoint-fpm.sh
COPY docker/config/entrypoint.sh /entrypoint.sh
COPY docker/config/nginx/nginx.sh /entrypoint-nginx.sh

# Make entrypoints executable and fix the PHP version number
ARG PHP_VERSION="81"
RUN \
    chmod +x /entrypoint-fpm.sh /entrypoint.sh /entrypoint-nginx.sh && \
    # Replace PHPVERSION with the variable ${PHP_VERSION}
    sed -i "s/PHPVERSION/${PHP_VERSION}/g" /entrypoint-fpm.sh /etc/php${PHP_VERSION}/php-fpm.conf

WORKDIR /var/www
EXPOSE 8080
ENTRYPOINT [ "/entrypoint.sh" ]
CMD [ "/usr/local/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf" ]
