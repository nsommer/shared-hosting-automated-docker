#!/bin/sh

set -e

# Create /run/php directory if it doesn't exist
if [ ! -d /run/php ]; then
    mkdir -p /run/php
fi

# Start fpm
/usr/bin/php-fpm -R -F -y /etc/phpPHPVERSION/php-fpm.conf
